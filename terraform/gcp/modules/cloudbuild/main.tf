resource "google_cloudbuild_trigger" "filename-trigger" {
  name = var.gcp_cloudbuild_trigger_name
  project = var.gcp_project
  provider = google-beta

  github {
    name = var.bitbucket_repo_name
    owner = var.bitbucket_repo_owner
    pull_request {
      branch = var.bitbucket_branch_name
    }
  }

  substitutions = {
    _APP_ENV = var.app_env
    _APP_CLUSTER = var.app_cluster
    _APP_NAMESPACE = var.app_namespace
    _APP_REGION = var.app_region
  }

  filename = var.gcp_cloudbuild_filename
}