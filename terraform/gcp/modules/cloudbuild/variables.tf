variable "bitbucket_branch_name" {
  type = string
  description = "Bitbucket branch name"
}

variable "bitbucket_repo_name" {
  type = string
  description = "Bitbucket repository name"
}

variable "bitbucket_repo_owner" {
  type = string
  description = "Bitbucket repository owner"
}

variable "app_env" {
  type = string
  description = "Application Env (production/staging/development)"
}

variable "app_cluster" {
  type = string
  description = "Application Kubernetes Cluster"
}

variable "app_namespace" {
  type = string
  description = "Application Kubernetes Namespace"
}

variable "app_region" {
  type = string
  description = "Application Kubernetes Node Region"
}

variable "gcp_cloudbuild_filename" {
  type = string
  description = "Folder that contains cloudbuild.yaml"
}

variable "gcp_project_id" {
  type = string
  description = "Google Cloud Platform Project Id (deliverycenter-prod/deliverycenter-stag)"
}

variable "gcp_project" {
  type = string
  description = "Google Cloud Platform Project (deliverycenter-prod/deliverycenter-stag)"
}

variable "gcp_cloudbuild_trigger_name" {
  type = string
  description = "Cloud Build Trigger Name - Generally same as bitbucket repository name"
}